const db = require('./dbConnect');
const computeYear = require('../utils/computeYear');

module.exports = async function checkClassic(carMake, year) {
	const yearChoices = computeYear(year);
	const { id } = await db.pg.query(`
		SELECT id FROM X WHERE name = '${carMake.toLowerCase()}'
	`)
		.then(result => result[0]);

	return db.pg.query(`
		SELECT id, name
		FROM X
		WHERE brand_id = '${id}'
		AND min_year <= ${yearChoices[0]}
		AND min_year <= ${yearChoices[1]}
		AND (max_year IS NULL OR (max_year >= ${yearChoices[0]} AND max_year >= ${yearChoices[1]}))
		AND (name = 'classic' OR name = 'all')
	`);
};
