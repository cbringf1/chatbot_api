const similarity = require('similarity');

const db = require('./dbConnect');

module.exports = function checkMake(carMake) {
	console.log("QUERYING MAKE", carMake);

	return db.pg.query(`
		SELECT id, name
		FROM X
	`)
		.then(result => {
			return result
				.filter(r => similarity(r.name, carMake.toLowerCase()) >= db.minSimilarity)
				.map(r => ({ ...r, similarity: similarity(r.name, carMake.toLowerCase()) }))
				.sort((r1, r2) => (r2.similarity - r1.similarity) / Math.abs(r2.similarity - r1.similarity))
				.slice(0, db.suggestionCount);
		});
};
