const similarity = require('similarity');

const db = require('./dbConnect');

module.exports = async function checkModel(carModel, carMake, year) {
	console.log("QUERYING MODEL DB FOR", carMake, carModel, year);

	const {id} = await db.pg.query(`
		SELECT id FROM X WHERE name = '${carMake.toLowerCase()}'
	`).then(result => result[0]);

	return db.pg.query(`
		SELECT id, name
		FROM X
		WHERE brand_id = '${id}'
	`)
		.then(result => {
			return result
				.filter(r => similarity(r.name, carModel.toLowerCase()) >= db.minSimilarity)
				.map(r => ({ ...r, similarity: similarity(r.name, carModel.toLowerCase()) }))
				.sort((r1, r2) => (r2.similarity - r1.similarity))
				.slice(0, db.suggestionCount);
		});
};
