const db = require('./dbConnect');

module.exports = function validateCar(year, make, model) {
	return db.pg.query(`
		SELECT X.id
		FROM X
		JOIN X ON X.id = X.brand_id
		WHERE brand.name = '${make}'
			AND (X.name = '${model}' OR X.name = 'all' OR X.name = 'classic')
			AND X.min_year <= '${year}'
			AND (max_year IS NULL OR max_year >= '${year}')
	`)
	.then(result => result);
};
