'use_strict'

const express = require('serverless-express/express');

const router = express.Router();

const dbCheckClassic = require('../db/checkClassic');
const checkClassicRes = require('../responses/checkClassic')

router.post('/', (req, res, next) => {
	const data = req.body.result.sessionParameters;

	dbCheckClassic(data.carMake, data.year)
		.then(result => {
			const checkClassicResponse = checkClassicRes(result);

			return checkClassicResponse ? res.send(checkClassicResponse) : res.end();
		})
		.catch(e => res.send(JSON.stringify({
			responses:[
			{
					type: "text",
					delay: 2000,
					messages:[`Error ${e}`]
			}]
		})));
});

module.exports = router;
