const express = require('serverless-express/express');

const router = express.Router();

const dbCheckMake = require('../db/checkMake');
const suggestionRes = require('../responses/carMakeSuggestion');
const perfectMatchResponse = require('../responses/perfectMatch');

router.post('/', (req, res, next) => {
	const data = req.body.result.sessionParameters;

	console.log("CHECKING MAKE BODY", JSON.stringify(req.body, null, 4));

	dbCheckMake(data.carMake)
		.then(result => {
			if(result[0].similarity !== 1){
				return res.send(suggestionRes(result[0].name));
			}
			else {
				return res.send(perfectMatchResponse("suggestedMake", result[0].name, "X"));
			}
		})
		.catch(e => res.send(JSON.stringify({
			responses:[
			{
					type: "text",
					delay: 2000,
					messages:[`Error ${e}`]
			}]
		})));
});

module.exports = router;
