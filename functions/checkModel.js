const express = require('serverless-express/express');

const router = express.Router();

const dbCheckModel = require('../db/checkModel');
const carModelSuggestionRes = require('../responses/carModelSuggestion');
const perfectMatchResponse = require('../responses/perfectMatch');

router.post('/', (req, res, next) => {
	const data = req.body.result.sessionParameters;

	console.log("CHECK MODEL BODY ", JSON.stringify(req.body, null, 4));

	dbCheckModel(data.carModel, data.suggestedMake ? data.suggestedMake : data.carMake, data.year)
		.then(result => {
			if (result[0].similarity !== 1)
				return res.send(carModelSuggestionRes(result[0].name));
			else
				return res.send(perfectMatchResponse("suggestedModel", result[0].name, "X"));
		})
		.catch(e => res.send(JSON.stringify({
			responses:[
			{
					type: "text",
					delay: 2000,
					messages:[`Error ${e}`]
			}]
		})));
});

module.exports = router;
