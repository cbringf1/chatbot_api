'use_strict'

const express = require('serverless-express/express');

const router = express.Router();

const path = require('path');

router.get('/', (req, res, next) => {
	res.sendFile(path.join(__dirname, "../views", 'index.html'));
});

module.exports = router;
