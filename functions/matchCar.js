const express = require('serverless-express/express');

const router = express.Router();

const formatYear = require('../helpers/dateHelper');

const dbValidateCar = require('../db/validateCar');
const dbCheckMake = require('../db/checkMake');
const dbCheckModel = require('../db/checkModel');

const suggestionResponse = require('../responses/carInfoSuggestion');
const perfectMatch = require('../responses/perfectMatch');
const redirectResponse = require('../responses/redirectResponse');
const invalidYearResponse = require('../responses/yearErrorResponse');

router.post('/', (req, res, next) => {
	const data = req.body.result.sessionParameters;
	const info = req.body.result.resolvedQuery.split(" ");

	console.log("MATCHING CAR INFO", JSON.stringify(req.body, null, 4));

	// try to parse first token as year
	let year = formatYear(info.shift());

	// look for the year in the rest of the tokens if needed
	while (!year && info.length > 0){
		year = formatYear(info.shift());
	}

	if (year)
		console.log("YEAR IS:", year);
	else
		return res.send(invalidYearResponse());

	// get the make
	let make = info.shift();

	console.log("MAKE IS:", make);

	// get a make suggestion
	if(year && make){
		dbCheckMake(make).then(result => {
			let suggestedMake = result[0];

			console.log("SUGGESTED MAKE IS:", suggestedMake);

			// if suggestedMake is not a perfect match and it's a 2 words make and user input has more than 1 word left
			// then it's likely that the next word is part of the make too
			if (suggestedMake.name.split(" ").length > 1 && info.length > 1){
				make += " " + info.shift();

				if (suggestedMake.name === make)
					suggestedMake.similarity = 1;
			}

			// get the model
			let model = info.join(" ");

			if (!model){
				return res.send(redirectResponse("the year and the make", 'X'));
			}

			// check model
			dbCheckModel(model, suggestedMake.name, year).then(modelResponse => {
				console.log("SUGGESTED MODEL IS:", modelResponse[0]);

				console.log("SUGGESTION IS:", year, suggestedMake.name, modelResponse[0].name);

				if (year && suggestedMake.similarity == 1 && modelResponse[0].similarity == 1){
					// return perfect match
					return res.send(perfectMatch(year.toString(), suggestedMake.name, modelResponse[0].name, 'X'));
				}
				else{
					// return suggestion
					return res.send(suggestionResponse(
						year.toString(),
						suggestedMake.name,
						modelResponse[0].name == "all" || modelResponse[0].name == "classic" ? "" : modelResponse[0].name));
				}
			});

		});
	}
	else{
		return res.send(redirectResponse("the year", 'X'));
	}
});

module.exports = router;
