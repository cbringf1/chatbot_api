const express = require('serverless-express/express');

const router = express.Router();

const dbValidateCar = require('../db/validateCar');
const validationResponse = require('../responses/validateCarResponse');

router.post('/', (req, res, next) => {
	const data = req.body.result.sessionParameters;

	console.log("VALIDATING CAR BODY", JSON.stringify(req.body, null, 4));

	dbValidateCar(data.year, data.suggestedMake, data.suggestedModel)
		.then(result => {
			if(result.length > 0) {
				// we are interested
				return res.send(validationResponse(data.year, data.suggestedMake, data.suggestedModel, true));
			}
			else {
				// we are not interested
				return res.send(validationResponse(data.year, data.suggestedMake, data.suggestedModel, false));
			}
		})
		.catch(e => res.send(JSON.stringify({
			responses:[
			{
					type: "text",
					delay: 2000,
					messages:[`Error ${e}`]
			}]
		})));
});

module.exports = router;
