const express = require('serverless-express/express');

const router = express.Router();

const yearErrorResponse = require('../responses/yearErrorResponse');
const yearResponse = require('../responses/yearResponse');

const formatYear = require('../helpers/dateHelper');

router.post('/', (req, res, next) => {
	const data = req.body.result.sessionParameters;

	console.log("VALIDATING YEAR BODY", JSON.stringify(req.body, null, 4));

	let formatedYear = formatYear(data.year);

	if (formatYear)
		return res.send(yearResponse(formatedYear));
	else
		return res.send(yearErrorResponse());
});

module.exports = router;
