const handler = require('serverless-express/handler');
const express = require('serverless-express/express');

const bodyParser = require('body-parser');
const url = require('url');
const querystring = require('querystring');

const db = require('./db/dbConnect');
const checkMake = require('./functions/checkMake');
const checkModel = require('./functions/checkModel');
const checkClassic = require('./functions/checkClassic');
const validateCar = require('./functions/validateCar');
const validateYear = require('./functions/validateYear');
const home = require('./functions/home');
const matchCar = require('./functions/matchCar');

const token = 'token.sfsdfdsf09809asdsad987sa';

const app = express();

app.use(bodyParser.json());

app.use('/checkMake', checkMake);
app.use('/checkClassic', checkClassic);
app.use('/checkModel', checkModel);
app.use('/validateCar', validateCar);
app.use('/validateYear', validateYear);
app.use('/home', home);
app.use('/matchCar', matchCar);

app.use((req, res, next) => {
	const parsed = url.parse(req.url);

	req.url = parsed.pathname;
	req.query = querystring.parse(parsed.query);

	if(req.method === 'GET') {
		return res.end(req.query.challenge);
	}

	next();
});

exports.api = handler(app);
