module.exports = function formatYear(yearString) {
	// try to parse the year string
	let year = parseInt(yearString);

	// if couldn not parse
	if (!year) return "";

	// check if it's in a valid year range
	if (year < 0 || year.toString().length > 4) return "";

	// get current year for comparison
	let currentYear = new Date().getFullYear();

	// if year is greater than current year report as invalid
	if (year > currentYear) return "";

	// if it's a 2 digits year => format it
	if (year.toString().length <= 3)
		year += year <= currentYear - 2000 ? 2000 : 1900;

	return year;
};
