module.exports = (year, make, model) => {
	return {
		responses: [
			{
				type: "setAttributes",
				elements: [
					{
						name: "year",
						action: "set",
						value: year
					}
				]
			},
			{
				type: "setAttributes",
				elements: [
					{
						name: "suggestedMake",
						action: "set",
						value: make
					}
				]
			},
			{
				type: "setAttributes",
				elements: [
					{
						name: "suggestedModel",
						action: "set",
						value: model
					}
				]
			},
			{
				type: "goto",
				interactionId: "X"
			}
		]
	};
};
