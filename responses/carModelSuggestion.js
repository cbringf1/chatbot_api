const uuid = require('uuid/v1')

module.exports = (name) => {
	return {
		responses: [
			{
				type: "quickReplies",
				delay: 2000,
				title: `Maybe you meant ... ${name}`,
				buttons: [
					{
						id: uuid(),
						type: "goto",
						title: "Yes",
						value: "X"
					},
					{
						id: uuid(),
						type: "goto",
						title: "No",
						value: "X"
					}
				]
			},
			{
				type: "setAttributes",
				elements: [
				  {
					name: "suggestedModel",
					action: "set",
					value: name

				  }
				]
			}
		]
	};
};
