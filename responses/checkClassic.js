const uuid = require('uuid/v1')

module.exports = (classicList) => {
	return classicList.length > 0 ?
		{
			responses: [
				{
					type: "quickReplies",
					delay: 2000,
					title: "👋 Would you like to be transferred to an agent?",
					buttons: [
						{
							id: uuid(),
							type: "goto",
							title: "👩‍💼Talk with agent",
							value: "X"
						},
						{
							id: uuid(),
							type: "goto",
							title: "📩Close Chat",
							value: "X"
						}
					]
				}
			]
		} :
		null;
};
