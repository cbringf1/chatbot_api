module.exports = (text, targetId) => {
	return {
		responses: [
			{
				type: "text",
				messages: [`Sorry, we need something more than just ${text}.`]
			},
			{
				type: "goto",
				interactionId: targetId
			}
		]
	};
};
