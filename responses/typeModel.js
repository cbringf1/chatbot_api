const uuid = require("uuid");

module.exports = () => {
	return {
		responses: [
			{
				type: "button",
				delay: 2000,
				buttons: [
					{
						id: uuid(),
						type: "goto",
						title: "Type model",
						value: "X"
					},
					{
						id: uuid(),
						type: "goto",
						title: "Bo Back",
						value: "X"
					},
					{
						id: uuid(),
						type: "goto",
						title: "📩End Chat",
						value: "X"
					}
				]
			}
		]
	};
};
