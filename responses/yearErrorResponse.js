module.exports = () => {
	return {
		responses: [
			{
				type: "text",
				messages: [`Sorry, year is in an invalid format.`]
			},
			{
				type: "goto",
				interactionId: "X"
			}
		]
	};
};
