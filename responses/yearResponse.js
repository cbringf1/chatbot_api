const uuid = require('uuid');

module.exports = (years) => {
	return years[0] === years[1] ? {
		responses: [
			{
				type: "setAttributes",
				elements: [
					{
						name: "year",
						action: "set",
						value: years[0].toString()
					}
				]
			},
			{
				type: "goto",
				interactionId: "5db9c8b4e8ff610007b4bb5b"
			}
		]
	} : {
			responses: [
				{
					type: "quickReplies",
					delay: 2000,
					buttons: [
						{
							id: uuid(),
							type: "goto",
							title: `${years[0]}`,
							value: "5dbc80cccb1d600007ac2b7d"
						},
						{
							id: uuid(),
							type: "goto",
							title: `${years[1]}`,
							value: "5dbc80cccb1d600007ac2b7d"
						}
					]
				}
			]
		};
};
