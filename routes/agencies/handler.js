const controller = require("./controller");
const responses = require("../../utils/io/handler_response");

const KEY_NAME = controller.KEY_NAME;

module.exports = {
	create: async (req, res) => {
		if (!req.body.users) {
			req.body.users = [];
		}
		const result = await controller.create(req.body.agency, req.body.users);

		if (!result.error) return responses.created(res, result.data);
		return responses.error(res, result.error);
	},
	delete: async (req, res) => {
		const result = await controller.delete(req.query[KEY_NAME]);

		if (!result.error) return responses.noContent(res);
		return responses.error(res, result.error);
	},
	update: async (req, res) => {
		const result = await controller.update(req.query[KEY_NAME], req.body);

		if (!result.error) return responses.ok(res, result.data);
		return responses.error(res, result.error);
	},
	findOne: async (req, res) => {
		const result = await controller.findOne(req.query[KEY_NAME]);

		if (!result.error) {
			if (!result.data) return responses.notFound(res, result.data);
			return responses.ok(res, result.data);
		}
		return responses.error(res, result.error);
	},
	getAll: async (req, res) => {
		try {
			const result = await controller.getAll(req.user.agencyId);

			if (!result.error) {
				return responses.ok(res, result.data);
			}
			return responses.error(res, result.error);
		} catch (error) {
			return responses.error(res, error);
		}
	},
	consume: async (req, res) => {
		const result = await controller.consume(
			req.query[KEY_NAME],
			req.body.credit
		);

		if (!result.error) {
			return responses.ok(res, result.data);
		}
		return responses.error(res, result.error);
	},
	credit: async (req, res) => {
		const result = await controller.credit(
			req.query[KEY_NAME],
			req.user.agencyId,
			req.user.username,
			req.body.credit
		);

		if (!result.error) {
			return responses.ok(res, result.data);
		}
		return responses.error(res, result.error);
	}
};
