"use_strict";

const express = require("serverless-express/express");

const router = express.Router();
const usrMid = require("../../middlewares/users");
const mid = require("../../middlewares/agencies");
const img = require("../../middlewares/transformImageData");
const handler = require("./handler");

router.get("/", usrMid.isLoggedIn, mid.find, (req, res) => {
	handler.getAll(req, res);
});

router.get("/byname", usrMid.isLoggedIn, mid.findOne, (req, res) => {
	handler.findOne(req, res);
});

router.post(
	"/",
	usrMid.isLoggedIn,
	usrMid.isAdmin,
	mid.create,
	img.transformImage,
	(req, res) => {
		handler.create(req, res);
	}
);

router.put(
	"/byname",
	usrMid.isLoggedIn,
	usrMid.isAdmin,
	mid.update,
	img.transformImage,
	(req, res) => {
		handler.update(req, res);
	}
);

router.post(
	"/credit/byname",
	usrMid.isLoggedIn,
	usrMid.isAdmin,
	mid.credit,
	(req, res) => {
		handler.credit(req, res);
	}
);

router.put(
	"/consume/byname",
	usrMid.isLoggedIn,
	usrMid.isAdmin,
	mid.changeCredit,
	(req, res) => {
		handler.consume(req, res);
	}
);

router.delete(
	"/byname",
	usrMid.isLoggedIn,
	usrMid.isAdmin,
	mid.destroy,
	(req, res) => {
		handler.delete(req, res);
	}
);

module.exports = router;
