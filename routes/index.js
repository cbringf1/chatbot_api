var express = require("serverless-express/express");
var router = express.Router();

router.get("/", function(req, res, next) {
	res.status(200).json({ message: "api  working as expected ! v1.99.7e" });
});

module.exports = router;
