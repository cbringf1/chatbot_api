module.exports = (year) => {
	if(year / 100 < 1) {
		const minYear = 1900 + year;
		const maxYear = 2000 + year;

		return [
			minYear,
			maxYear > new Date().getFullYear() ? minYear : maxYear
		];
	}
	else {
		return [year, year];
	}
};
